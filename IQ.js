var IQ = cc.Class.extend(
{
	level: 0,
	parent: null,
	num: 0,
	badseed: null,
	explain: "unknown",
	lang: null,

	ctor:function(_parent)
	{
		this.parent = _parent;
		this.num = this.parent.size.x * this.parent.size.y;
		this.lang = LANG_ENG;
		this.rotateObj = null;
		
		cc.log("parent=" + _parent + ", num=" + this.num + ",lang=" + this.lang);
	},
	
	test:function()
	{
		cc.log("test from IQ");
	},
	
	setLang: function( _lang )
	{
		this.lang = _lang;
	},
	
	generate:function()
	{
		return null;
	},
	
	onStart:function()
	{
		// This had been change to object
	},
	
	onComplete:function()
	{
		//cc.log("complete " + this.isVisible());
		this.setVisible( true );
	}
}
);

var IQEngineFibonacci = IQ.extend(
{
	level: 1,

	ctor:function(_parent)
	{
		this._super(_parent);
		//cc.log("level=" + this.level +", parent=" + this.parent);
		this.explain = (this.lang==LANG_CHS) ? "斐波那契数列" : "Fibonacci sequence numbers";
	},
	
	test:function()
	{
		cc.log("test from IQFibonacci");
	},
	
	generate:function()
	{
		var seed = Math.floor((Math.random()*10)+2);
		//seed = 12;
		var series = [];
		var result = [];
		var i;
		for(i=0; i<this.num; i++)
		{
			series.push(Math.nthFibonacci(seed+i));
		}
		this.badseed=Math.floor((Math.random()*this.num));
		var min=Math.nthFibonacci(seed+this.badseed-1);
		//cc.log(min);
		var max=Math.nthFibonacci(seed+this.badseed+1);
		//cc.log(max);
		var bad=-1;
		do
		{
			bad = Math.floor((Math.random()*(max-min))+min);
		}while(bad==Math.nthFibonacci(this.badseed+seed));
		series[this.badseed] = bad;
		
		var maxDigits = 1;
		for(i = 0; i<series.length; i++) 
		{
			cc.log(series[i]);
			digits = series[i].toString().length;
			if(maxDigits<digits) maxDigits = digits;
		}
		for(i = 0; i<series.length; i++) 
		{
			//var txtNum = cc.LabelTTF.create(""+series[i],  'Arial', this.parent.cellSize.x, cc.size(this.parent.cellSize), cc.TEXT_ALIGNMENT_CENTER);
			var txtNum = cc.LabelTTF.create(""+series[i], "Arial", Math.floor(this.parent.cellSize.x/2/maxDigits));
			txtNum.setAnchorPoint( cc.p(0.5, 0.5) );
			result.push(txtNum);
		}
		
		return result;
	}
}
);

var IQEngineNumSeq = IQ.extend(
{
	level: 1,

	ctor:function(_parent)
	{
		this._super(_parent);
		cc.log("level=" + this.level +", parent=" + this.parent);
		this.explain = (this.lang==LANG_CHS) ? "数字应该是连续的" : "Sequence numbers";
	},
	
	test:function()
	{
		cc.log("test from IQNumSeq");
	},
	
	generate:function()
	{
		var seed = Math.floor((Math.random()*96)+2);
		//seed = 12;
		var series = [];
		var result = [];
		var i;
		for(i=0; i<this.num; i++)
		{
			series.push(seed+i);
		}
		this.badseed=Math.floor((Math.random()*this.num));
		var min=(seed+this.badseed-10);
		//cc.log(min);
		var max=(seed+this.badseed+10);
		//cc.log(max);
		var bad=-1;
		do
		{
			bad = Math.floor((Math.random()*(max-min))+min);
		}while(bad==(this.badseed+seed));
		series[this.badseed] = bad;
		
		var maxDigits = 1;
		for(i = 0; i<series.length; i++) 
		{
			cc.log(series[i]);
			digits = series[i].toString().length;
			if(maxDigits<digits) maxDigits = digits;
		}
		for(i = 0; i<series.length; i++) 
		{
			//var txtNum = cc.LabelTTF.create(""+series[i],  'Arial', this.parent.cellSize.x, cc.size(this.parent.cellSize), cc.TEXT_ALIGNMENT_CENTER);
			var txtNum = cc.LabelTTF.create(""+series[i], "Arial", Math.floor(this.parent.cellSize.x/2/maxDigits));
			txtNum.setAnchorPoint( cc.p(0.5, 0.5) );
			result.push(txtNum);
		}
		
		return result;
	}
}
);

var IQEngineNumSeqStep = IQ.extend(
{
	level: 1,
	step: 0,

	ctor:function(_parent)
	{
		this._super(_parent);
		this.step = Math.floor((Math.random()*8)+2);
		this.explain = (this.lang==LANG_CHS) ? "两数字之间间隔应该相等" : "Arithmetic sequences";
		cc.log("level=" + this.level +", step=" + this.step);
	},
	
	test:function()
	{
		cc.log("test from IQNumSeqStep");
	},
	
	generate:function()
	{
		var seed = Math.floor((Math.random()*90)+2);
		//seed = 12;
		var series = [];
		var result = [];
		var i;
		for(i=0; i<this.num; i++)
		{
			var cell=seed+this.step*i;
			series.push(cell);
		}
		this.badseed=Math.floor((Math.random()*this.num));
		var min=(seed+this.badseed-this.step);
		//cc.log(min);
		var max=(seed+this.badseed+this.step);
		//cc.log(max);
		var bad=-1;
		do
		{
			bad = Math.floor((Math.random()*(max-min))+min);
		}while(bad==(this.badseed+seed+this.step));
		series[this.badseed] = bad;
		
		var maxDigits = 1;
		for(i = 0; i<series.length; i++) 
		{
			cc.log(series[i]);
			digits = series[i].toString().length;
			if(maxDigits<digits) maxDigits = digits;
		}
		for(i = 0; i<series.length; i++) 
		{
			//var txtNum = cc.LabelTTF.create(""+series[i],  'Arial', this.parent.cellSize.x, cc.size(this.parent.cellSize), cc.TEXT_ALIGNMENT_CENTER);
			var txtNum = cc.LabelTTF.create(""+series[i], "Arial", Math.floor(this.parent.cellSize.x/2/maxDigits));
			txtNum.setAnchorPoint( cc.p(0.5, 0.5) );
			result.push(txtNum);
		}
		
		return result;
	}
}
);

var IQEngineNumOddEven = IQ.extend(
{
	level: 1,
	odds: false,

	ctor:function(_parent)
	{
		this._super(_parent);
		this.odds = (Math.random()<0.5 ? true : false);
		this.explain = (this.lang==LANG_CHS) ? "所有数字都应是"+(this.odds?"单数":"双数") : "Numbers should all be "+(this.odds?"odds":"even");
		cc.log("level=" + this.level +", odds=" + this.odds);
	},
	
	test:function()
	{
		cc.log("test from IQNumOddEven");
	},
	
	generate:function()
	{
		var seed = Math.floor((Math.random()*99)+2);
		//seed = 12;
		var series = [];
		var result = [];
		var i;
		for(i=0; i<this.num; i++)
		{
			var cell = Math.floor(Math.random()*95);
			if((this.odds && cell%2===0 ) || (!this.odds && cell%2===1)) 
				cell++;
			series.push(cell);
		}
		this.badseed=Math.floor((Math.random()*this.num));
	
		series[this.badseed]++;
		
		var maxDigits = 1;
		for(i = 0; i<series.length; i++) 
		{
			cc.log(series[i]);
			digits = series[i].toString().length;
			if(maxDigits<digits) maxDigits = digits;
		}
		for(i = 0; i<series.length; i++) 
		{
			//var txtNum = cc.LabelTTF.create(""+series[i],  'Arial', this.parent.cellSize.x, cc.size(this.parent.cellSize), cc.TEXT_ALIGNMENT_CENTER);
			var txtNum = cc.LabelTTF.create(""+series[i], "Arial", Math.floor(this.parent.cellSize.x/2/maxDigits));
			txtNum.setAnchorPoint( cc.p(0.5, 0.5) );
			result.push(txtNum);
		}

		return result;
	}
}
);

var IQEngineNum = IQ.extend(
{
	level: 1,

	ctor:function(_parent)
	{
		this._super(_parent);
		cc.log("level=" + this.level +", parent=" + this.parent);
		this.explain = (this.lang==LANG_CHS) ? "数字应该是连续的" : "Numbers should be in sequential order";
	},
	
	test:function()
	{
		cc.log("test from IQNumSeq");
	},
	
	generate:function()
	{
		var seed = Math.floor((Math.random()*96)+2);
		//seed = 12;
		var series = [];
		var result = [];
		var i;
		for(i=0; i<this.num; i++)
		{
			series.push(seed+i);
		}
		this.badseed=Math.floor((Math.random()*this.num));
		var min=(seed+this.badseed-10);
		//cc.log(min);
		var max=(seed+this.badseed+10);
		//cc.log(max);
		var bad=-1;
		do
		{
			bad = Math.floor((Math.random()*(max-min))+min);
		}while(bad==(this.badseed+seed));
		series[this.badseed] = bad;
		
		var maxDigits = 1;
		for(i = 0; i<series.length; i++) 
		{
			cc.log(series[i]);
			digits = series[i].toString().length;
			if(maxDigits<digits) maxDigits = digits;
		}
		for(i = 0; i<series.length; i++) 
		{
			//var txtNum = cc.LabelTTF.create(""+series[i],  'Arial', this.parent.cellSize.x, cc.size(this.parent.cellSize), cc.TEXT_ALIGNMENT_CENTER);
			var txtNum = cc.LabelTTF.create(""+series[i], "Arial", Math.floor(this.parent.cellSize.x/2/maxDigits));
			txtNum.setAnchorPoint( cc.p(0.5, 0.5) );
			result.push(txtNum);
		}
		
		return result;
	}
}
);

var IQEngineNumPower = IQ.extend(
{
	level: 1,

	ctor:function(_parent)
	{
		this._super(_parent);
		cc.log("level=" + this.level + ", power of seq");
		this.explain = (this.lang==LANG_CHS) ? "平方数" : "Square numbers";
	},
	
	test:function()
	{
		cc.log("test from IQNumPower");
	},
	
	generate:function()
	{
		var seed = Math.floor((Math.random()*6)+1);
		//seed = 12;
		var series = [];
		var result = [];
		var i;
		for(i=0; i<this.num; i++)
		{
			var number = Math.pow(seed+i, 2); 
			series.push(number);
		}
		this.badseed=Math.floor((Math.random()*this.num));
		var min=Math.pow((seed+this.badseed-3), 2);
		//cc.log(min);
		var max=Math.pow((seed+this.badseed+3), 2);
		//cc.log(max);
		var bad=-1;
		do
		{
			bad = Math.floor((Math.random()*(max-min))+min);
		}while(bad==series[this.badseed]);
		series[this.badseed] = bad;
		
		var maxDigits = 1;
		for(i = 0; i<series.length; i++) 
		{
			cc.log(series[i]);
			digits = series[i].toString().length;
			if(maxDigits<digits) maxDigits = digits;
		}
		for(i = 0; i<series.length; i++) 
		{
			//var txtNum = cc.LabelTTF.create(""+series[i],  'Arial', this.parent.cellSize.x, cc.size(this.parent.cellSize), cc.TEXT_ALIGNMENT_CENTER);
			var txtNum = cc.LabelTTF.create(""+series[i], "Arial", Math.floor(this.parent.cellSize.x/2/maxDigits));
			txtNum.setAnchorPoint( cc.p(0.5, 0.5) );
			result.push(txtNum);
		}
		
		return result;
	}
}
);

var IQEngineNumPowerMinus = IQ.extend(
{
	level: 1,

	ctor:function(_parent)
	{
		this._super(_parent);
		cc.log("level=" + this.level);
		this.explain = (this.lang==LANG_CHS) ? "平方数减1" : "Square numbers minus 1";
	},
	
	test:function()
	{
		cc.log("test from IQNumPowerMinus");
	},
	
	generate:function()
	{
		var seed = Math.floor((Math.random()*6)+1);
		//seed = 12;
		var series = [];
		var result = [];
		var i;
		for(i=0; i<this.num; i++)
		{
			var number = Math.pow(seed+i, 2)-1; 
			series.push(number);
		}
		this.badseed=Math.floor((Math.random()*this.num));
		var min=Math.pow((seed+this.badseed-3), 2)-1;
		//cc.log(min);
		var max=Math.pow((seed+this.badseed+3), 2)-1;
		//cc.log(max);
		var bad=-1;
		do
		{
			bad = Math.floor((Math.random()*(max-min))+min);
		}while(bad==series[this.badseed]);
		series[this.badseed] = bad;
		
		var maxDigits = 1;
		for(i = 0; i<series.length; i++) 
		{
			cc.log(series[i]);
			digits = series[i].toString().length;
			if(maxDigits<digits) maxDigits = digits;
		}
		for(i = 0; i<series.length; i++) 
		{
			//var txtNum = cc.LabelTTF.create(""+series[i],  'Arial', this.parent.cellSize.x, cc.size(this.parent.cellSize), cc.TEXT_ALIGNMENT_CENTER);
			var txtNum = cc.LabelTTF.create(""+series[i], "Arial", Math.floor(this.parent.cellSize.x/2/maxDigits));
			txtNum.setAnchorPoint( cc.p(0.5, 0.5) );
			result.push(txtNum);
		}
		
		return result;
	}
}
);

var IQEngineNumPowerOf2 = IQ.extend(
{
	level: 1,

	ctor:function(_parent)
	{
		this._super(_parent);
		cc.log("level=" + this.level + ", power of 2");
		this.explain = (this.lang==LANG_CHS) ? "2的平方" : "Power of 2";
	},
	
	test:function()
	{
		cc.log("test from IQNumPowerOf2");
	},
	
	generate:function()
	{
		var seed = Math.floor((Math.random()*5)+1);
		//seed = 12;
		var series = [];
		var result = [];
		var i;
		for(i=0; i<this.num; i++)
		{
			var number = Math.pow(2, seed+i); 
			series.push(number);
		}
		this.badseed=Math.floor((Math.random()*this.num));
		var min=Math.pow(2, (seed+this.badseed-2));
		//cc.log(min);
		var max=Math.pow(2, (seed+this.badseed+2));
		//cc.log(max);
		var bad=-1;
		do
		{
			bad = Math.floor((Math.random()*(max-min))+min);
		}while(bad==series[this.badseed]);
		series[this.badseed] = bad;
		
		var maxDigits = 1;
		for(i = 0; i<series.length; i++) 
		{
			cc.log(series[i]);
			digits = series[i].toString().length;
			if(maxDigits<digits) maxDigits = digits;
		}
		for(i = 0; i<series.length; i++) 
		{
			//var txtNum = cc.LabelTTF.create(""+series[i],  'Arial', this.parent.cellSize.x, cc.size(this.parent.cellSize), cc.TEXT_ALIGNMENT_CENTER);
			var txtNum = cc.LabelTTF.create(""+series[i], "Arial", Math.floor(this.parent.cellSize.x/2/maxDigits));
			txtNum.setAnchorPoint( cc.p(0.5, 0.5) );
			result.push(txtNum);
		}
		
		return result;
	}
}
);

var IQEngineNumTri = IQ.extend(
{
	level: 1,

	ctor:function(_parent)
	{
		this._super(_parent);
		cc.log("level=" + this.level);
		this.explain = (this.lang==LANG_CHS) ? "两数之差为序列数" : "Triangular Numbers";
	},
	
	test:function()
	{
		cc.log("test from IQNumSumSeq");
	},
	
	generate:function()
	{
		var seed = Math.floor((Math.random()*60)+1);
		//seed = 12;
		var series = [];
		var result = [];
		var i;
		var number = -1;
		var step = Math.floor((Math.random()*3)+1);
		for(i=0; i<this.num; i++)
		{
			if(number===-1) number = seed+i; 
			else number = number + i + step;
			series.push(number);
		}
		this.badseed=Math.floor((Math.random()*this.num));
		var min=series[this.badseed]+1;
		//cc.log(min);
		var max=series[this.badseed]+3;
		//cc.log(max);
		var bad=-1;
		do
		{
			bad = Math.floor((Math.random()*(max-min))+min);
		}while(bad==series[this.badseed]);
		series[this.badseed] = bad;
		
		var maxDigits = 1;
		for(i = 0; i<series.length; i++) 
		{
			cc.log(series[i]);
			digits = series[i].toString().length;
			if(maxDigits<digits) maxDigits = digits;
		}
		for(i = 0; i<series.length; i++) 
		{
			//var txtNum = cc.LabelTTF.create(""+series[i],  'Arial', this.parent.cellSize.x, cc.size(this.parent.cellSize), cc.TEXT_ALIGNMENT_CENTER);
			var txtNum = cc.LabelTTF.create(""+series[i], "Arial", Math.floor(this.parent.cellSize.x/2/maxDigits));
			txtNum.setAnchorPoint( cc.p(0.5, 0.5) );
			result.push(txtNum);
		}
		
		return result;
	}
}
);

var IQEngineNumCubic = IQ.extend(
{
	level: 1,

	ctor:function(_parent)
	{
		this._super(_parent);
		cc.log("level=" + this.level);
		this.explain = (this.lang==LANG_CHS) ? "立方数" : "Cube numbers";
	},
	
	test:function()
	{
		cc.log("test from IQNumCubic");
	},
	
	generate:function()
	{
		var seed = Math.floor((Math.random()*3)+1);
		//seed = 12;
		var series = [];
		var result = [];
		var i;
		for(i=0; i<this.num; i++)
		{
			var number = Math.pow(seed+i, 3); 
			series.push(number);
		}
		this.badseed=Math.floor((Math.random()*this.num));
		var min=Math.pow((seed+this.badseed-1), 3);
		//cc.log(min);
		var max=Math.pow((seed+this.badseed+1), 3);
		//cc.log(max);
		var bad=-1;
		do
		{
			bad = Math.floor((Math.random()*(max-min))+min);
		}while(bad==series[this.badseed]);
		series[this.badseed] = bad;
		
		var maxDigits = 1;
		for(i = 0; i<series.length; i++) 
		{
			cc.log(series[i]);
			digits = series[i].toString().length;
			if(maxDigits<digits) maxDigits = digits;
		}
		for(i = 0; i<series.length; i++) 
		{
			//var txtNum = cc.LabelTTF.create(""+series[i],  'Arial', this.parent.cellSize.x, cc.size(this.parent.cellSize), cc.TEXT_ALIGNMENT_CENTER);
			var txtNum = cc.LabelTTF.create(""+series[i], "Arial", Math.floor(this.parent.cellSize.x/2/maxDigits));
			txtNum.setAnchorPoint( cc.p(0.5, 0.5) );
			result.push(txtNum);
		}
		
		return result;
	}
}
);

var IQEngineNumTetrahedral = IQ.extend(
{
	level: 2,

	ctor:function(_parent)
	{
		this._super(_parent);
		cc.log("level=" + this.level);
		this.explain = (this.lang==LANG_CHS) ? "四面体数" : "Tetrahedral numbers";
	},
	
	test:function()
	{
		cc.log("test from IQNumTetrahedral");
	},
	
	generate:function()
	{
		var seed = Math.floor((Math.random()*3)+1);
		var series = [];
		var result = [];
		var i;
		for(i=0; i<this.num; i++)
		{
			var number = Math.nthTetrahedral(seed+i);
			series.push(number);
		}
		this.badseed=Math.floor((Math.random()*this.num));
		var min = this.badseed===0 ? 1 : series[this.badseed];
		//cc.log(min);
		var max = this.badseed===(series.length-1) ? Math.floor(series[this.badseed]*0.5) : series[this.badseed+1];
		//cc.log(max);
		var bad=-1;
		do
		{
			bad = Math.floor((Math.random()*(max-min))+min);
		}while(bad==series[this.badseed]);
		series[this.badseed] = bad;
		
		var maxDigits = 1;
		for(i = 0; i<series.length; i++) 
		{
			cc.log(series[i]);
			digits = series[i].toString().length;
			if(maxDigits<digits) maxDigits = digits;
		}
		for(i = 0; i<series.length; i++) 
		{
			//var txtNum = cc.LabelTTF.create(""+series[i],  'Arial', this.parent.cellSize.x, cc.size(this.parent.cellSize), cc.TEXT_ALIGNMENT_CENTER);
			var txtNum = cc.LabelTTF.create(""+series[i], "Arial", Math.floor(this.parent.cellSize.x/2/maxDigits));
			txtNum.setAnchorPoint( cc.p(0.5, 0.5) );
			result.push(txtNum);
		}
		
		return result;
	}
}
);

var IQEngineNumPI = IQ.extend(
{
	level: 2,

	ctor:function(_parent)
	{
		this._super(_parent);
		cc.log("level=" + this.level);
		this.explain = (this.lang==LANG_CHS) ? "圆周率" : "PI";
	},
	
	test:function()
	{
		cc.log("test from IQNumPI");
	},
	
	generate:function()
	{
		var p = "31415926535897932626";
		
		var seed = 0;
		var series = [];
		var result = [];
		var i;
		for(i=0; i<this.num; i++)
		{
			var number = Number(p.charAt(seed+i));
			series.push(number);
			//cc.log(number);
		}
		this.badseed=Math.floor((Math.random()*this.num));
		var bad=-1;
		do
		{
			bad = Math.floor((Math.random()*10));
		}while(bad==series[this.badseed]);
		series[this.badseed] = bad;
		
		var maxDigits = 1;
		for(i = 0; i<series.length; i++) 
		{
			cc.log(series[i]);
			digits = series[i].toString().length;
			if(maxDigits<digits) maxDigits = digits;
		}
		for(i = 0; i<series.length; i++) 
		{
			//var txtNum = cc.LabelTTF.create(""+series[i],  'Arial', this.parent.cellSize.x, cc.size(this.parent.cellSize), cc.TEXT_ALIGNMENT_CENTER);
			var txtNum = cc.LabelTTF.create(""+series[i], "Arial", Math.floor(this.parent.cellSize.x/2/maxDigits));
			txtNum.setAnchorPoint( cc.p(0.5, 0.5) );
			result.push(txtNum);
		}
		
		return result;
	}
}
);

var IQEngineAlpha = IQ.extend(
{
	level: 1,

	ctor:function(_parent)
	{
		this._super(_parent);
		cc.log("level=" + this.level);
		this.explain = (this.lang==LANG_CHS) ? "字母顺序" : "Alphabetic order";
	},
	
	test:function()
	{
		cc.log("test from IQAlpha");
	},
	
	generate:function()
	{
		var p = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
		var step =  Math.floor((Math.random()*3)+1);
		var seed = Math.floor((Math.random()*p.length-this.num*step));
		if(seed<0)
		{
			step=1;
			seed = Math.floor((Math.random()*p.length-this.num*step));
		}
		var series = [];
		var result = [];
		var i;
		for(i=0; i<this.num; i++)
		{
			var number = p.charAt(seed+i*step);
			series.push(number);
			//cc.log(number);
		}
		this.badseed=Math.floor((Math.random()*this.num));
		var bad=-1;
		do
		{
			bad = p.charAt(Math.random()*p.length);
		}while(bad==series[this.badseed]);
		series[this.badseed] = bad;
		
		var maxDigits = 1;
		for(i = 0; i<series.length; i++) 
		{
			cc.log(series[i]);
			digits = series[i].toString().length;
			if(maxDigits<digits) maxDigits = digits;
		}
		for(i = 0; i<series.length; i++) 
		{
			//var txtNum = cc.LabelTTF.create(""+series[i],  'Arial', this.parent.cellSize.x, cc.size(this.parent.cellSize), cc.TEXT_ALIGNMENT_CENTER);
			var txtNum = cc.LabelTTF.create(""+series[i], "Arial", Math.floor(this.parent.cellSize.x/2/maxDigits));
			txtNum.setAnchorPoint( cc.p(0.5, 0.5) );
			result.push(txtNum);
		}
		
		return result;
	}
}
);

//====================================

Math.nthFibonacci = function(num) 
{
	if (num > 2) {
		return Math.nthFibonacci(num - 2) + Math.nthFibonacci(num - 1);
    } else {             
        return 1;
    }
};

Math.nthTetrahedral = function(lim)
{  
	var tri_N   = 0;  // N'th triangular number
	var tetra_N = 0;  // N'th tetrahedral number

	for (var n = 1; n <= lim; n++)
	{  
		tri_N   += n;
		tetra_N += tri_N;
	}
	return tetra_N;
};

// Converts from degrees to radians.
Math.radians = function(degrees) {
  return degrees * Math.PI / 180;
};

// Converts from radians to degrees.
Math.degrees = function(radians) {
  return radians * 180 / Math.PI;
};
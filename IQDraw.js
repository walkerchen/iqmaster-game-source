var IQDraw = function(_root) {
	this.master = _root;
	
	this.size = cc.p(2,3);
	this.boardOccupy = cc.p(0.8, 0.7);
	this.cellOccupy = cc.p(0.95, 0.95);
	this.cellSize = cc.p(gWinSize.width*this.boardOccupy.x/this.size.x, gWinSize.height*this.boardOccupy.y/this.size.y);
	
	this.cellContentSize = cc.p(gWinSize.width*this.boardOccupy.x*this.cellOccupy.x/this.size.x, 
		gWinSize.height*this.boardOccupy.y*this.cellOccupy.y/this.size.y);
	
	// 变为正方形
	if(this.cellContentSize.x>this.cellContentSize.y) this.cellContentSize.x = this.cellContentSize.y;
	else this.cellContentSize.y = this.cellContentSize.x;
		
	this.cells = [];
	
	this.board = null;
};

IQDraw.prototype.play = function()
{
	var spos = cc.p(gWinSize.width/2, gWinSize.height/2);
	
	this.board = cc.LayerColor.create(cc.c4b(200,200,200,255), gWinSize.width*this.boardOccupy.x, gWinSize.height*this.boardOccupy.y);
	this.board.ignoreAnchorPointForPosition(false);
	this.board.setAnchorPoint( cc.p(0.5, 0.5) );
	this.board.setPosition( spos );
	this.master.rootNode.addChild(this.board);
	
	/*
	var n1 = cc.LabelTTF.create("TEST", "Arial", 20);
	n1.setAnchorPoint( cc.p(0.5, 0.5) );
	n1.setPosition( cc.p(0,0) );
	this.board.addChild(n1);
	*/
	
	/*	
	for(var i=0; i<this.size.x; i++)
	{
		for( var j=0; j<this.size.y; j++)
		{
			var obj = this.drawCircle(this.cellContentSize.x/2);

			obj.ignoreAnchorPointForPosition(true);
			obj.setAnchorPoint( cc.p(0, 0) );
			obj.setPosition( cc.p((i+0.5)*this.cellSize.x, (j+0.5)*this.cellSize.y) );
			
			this.board.addChild(obj);
			//var act = cc.ScaleTo.create( 1.0, 0.5);
			//obj.runAction(act);
		}
	}
	*/
	
	this.genIQ();
	
	//var sp = cc.Sprite.create("sun.png");
	//this.master.rootNode.addChild(sp);		
	//sp.setAnchorPoint( cc.p(0.5, 0.5) );
	
	//var dl = new DashedLineSprite(0,0,300,300,[30,10]);
	//this.master.rootNode.addChild(dl);		
};

IQDraw.prototype.genIQ = function()
{
	//var iq1 = new IQEngineFibonacci(this);
	//var iq1 = new IQEngineNumSeq(this);
	//var iq1 = new IQEngineGraphBox(this);
	
	//var iq1 = new IQEngineRotateBox(this, 0, 1);
	//var iq1 = new IQGraphRotateCombLineBox(this, 0, 1);
	//var iq1 = new IQEngineRotateCircle(this, 0.2, 0.9);
	//var iq1 = new IQEngineRotateKoch(this, -0.9, 0.9);
	//var iq1 = new IQEngineRotateComb(this, 0.2, 0.9);
	var iq1 = new IQEngineDextroLevoSpiral(this, -0.9, 0.9);
	
	this.presentIQ(iq1);
	
	/*
	var iq2 = new IQGraphLogo(this, -1, 1);
	var cell = iq2.newCell();
	cell.setAnchorPoint(cc.p(0, 0));
	cell.setPosition(cc.p(this.cellSize.x/2, this.cellSize.y/2));
	this.board.addChild(cell);
	*/
};

IQDraw.prototype.presentIQ = function(iqobj)
{
	var cells = iqobj.generate();

	var spos = cc.p(0, this.board.getContentSize().height);
	cc.log("y=" + this.board.getContentSize().height);
	for(var i=0; i<this.size.x; i++)
	{
		for( var j=0; j<this.size.y; j++)
		{
			//var obj = this.drawCircle(this.cellContentSize.x/2);
			var obj = cells[j*this.size.x+i];
			var opos = obj.getPosition();
			var pos = cc.p((i+0.5)*this.cellSize.x + opos.x + (this.cellSize.x - this.cellContentSize.x)/2, 
				spos.y-(j+0.5)*this.cellSize.y + opos.y + (this.cellSize.y - this.cellContentSize.y)/2);
			obj.setPosition(cc.p(0,0));
			
			//obj.rotate(1.0, 180);
			var holder = cc.LayerColor.create(cc.c4b(255, 255, 0, 255), this.cellContentSize.x, this.cellContentSize.y);
			holder.addChild(obj);
			//holder.ignoreAnchorPointForPosition(false);
			holder.setPosition( pos );
			holder.setAnchorPoint(cc.p(0.5,0.5));

			this.board.addChild(holder);
			
			//var act = cc.RotateBy.create( 1.0, 360);
			//obj.runAction(act);
		}
	}
	
	// Present the explain
	var fontSize = Math.floor(this.cellSize.x/2/4);
	var exp = cc.LabelTTF.create(iqobj.explain, "Arial", fontSize);
	exp.setAnchorPoint(cc.p(0.5, 0.5));
	exp.setPosition(cc.p(gWinSize.width/2, gWinSize.height/2 - this.board.getContentSize().height/2 - 1.5*fontSize) );
	this.master.rootNode.addChild(exp);
	
	cc.log("badseed=" + iqobj.badseed);
	cc.log(iqobj.explain);
};

IQDraw.prototype.findTouchedItem = function(touches, event)
{
	var loc = touches[0].getLocation();
    
	cc.log("findTouchedItem " + loc.x + "," + loc.y);
};

IQDraw.prototype.hello = function()
{
	var pen = new DrawUtil();
	
	var circle = pen.drawCircle(80);
	circle.setAnchorPoint( cc.p(0,0) );
	circle.setPosition( cc.p(gWinSize.width/2, gWinSize.height/2) );
	this.master.rootNode.addChild(circle);
	
	var rect = pen.drawRectangle(80, 200);
	rect.setAnchorPoint( cc.p(0,0) );
	rect.setPosition( cc.p(gWinSize.width/2, gWinSize.height/2-200) );

	this.master.rootNode.addChild(rect);
	
	var sp = cc.Sprite.create("sun.png");
	this.master.rootNode.addChild(sp);
};

// ------------- draw utility class -------------------

var DrawUtil = function()
{
	this.color = cc.c4f(0,1,0,1);
	this.fillColor = cc.c4f(0.3,0.3,0.3,1);
	this.border = 1;
};

DrawUtil.prototype.drawCircle = function(r)
{
	var precision = 0.02;
	var cir = 2 * Math.PI;
	var radius = r;
	
	var verts = [];
	
	var x=0, y=0;
	for (a = 0.0; a < cir; a += precision) {
		x = radius*Math.cos(a);
		y = radius*Math.sin(a);
		verts.push(cc.p(x,y));
	}
	var dn = cc.DrawNode.create();
	//dn.drawPolygon( verts, verts.length, cc.c4f(1.0,1.0,0,1.0), 5, cc.c4f(0.0,1.0,0,1.0) );
	dn.drawPoly( verts, this.fillColor, this.border, this.color );
	
	return dn;
};

DrawUtil.prototype.drawRectangle = function(w, h)
{
	var verts = [];
	
	verts.push(cc.p(0,0));
	verts.push(cc.p(w,0));
	verts.push(cc.p(w,h));
	verts.push(cc.p(0,h));
	
	var dn = cc.DrawNode.create();
	//dn.drawPolygon( verts, verts.length, cc.c4f(1.0,1.0,0,1.0), 5, cc.c4f(0.0,1.0,0,1.0) );
	dn.drawPoly( verts, this.fillColor, this.border, this.color );
	return dn;
};

DrawUtil.prototype.drawLine = function(p1, p2)
{
	var verts = [];
	
	verts.push(p1);
	verts.push(p2);
	
	var dn = cc.DrawNode.create();
	//dn.drawPolygon( verts, verts.length, cc.c4f(1.0,1.0,0,1.0), 5, cc.c4f(0.0,1.0,0,1.0) );
	dn.drawSegment( p1, p2, this.border, this.color);
	
	return dn;
};

DrawUtil.prototype.drawVertexs = function(verts, close)
{
	var dn = cc.DrawNode.create();
	//dn.drawPolygon( verts, verts.length, cc.c4f(1.0,1.0,0,1.0), 5, cc.c4f(0.0,1.0,0,1.0) );
	if(close===true)
	{
		dn.drawPoly( verts, this.fillColor, this.border, this.color );
	}
	else
	{
		for(var i=1; i<verts.length; i++)
		{
			//cc.log(verts[i].x + "," + verts[i].y);
			dn.drawSegment( verts[i-1], verts[i], this.border, this.color);
		}
	}
	return dn;
};

var IQLogo = cc.Class.extend(
{
	heading: 0,
	size: null,
	pos: cc.p(0,0),
	vertexs: null,
	
	ctor:function(_parent, _size)
	{
		this.size = _size;
		this.reset();
	},
	
	reset:function()
	{
		this.heading = 0;
		this.pos = cc.p(this.size.x/2, this.size.y/2);
		this.vertexs = [];
	},
	
	lt:function(degree)
	{
		this.heading -= degree;
		this.heading += 360;
		this.heading %= 360;
	},
	
	rt:function(degree)
	{
		this.heading += degree;
		this.heading %= 360;
	},
	
	fd:function(step)
	{
		this.pos = cc.p(Math.round(this.pos.x+step*Math.sin(Math.radians(this.heading))), 
			Math.round(this.pos.y+step*Math.cos(Math.radians(this.heading))));
		this.vertexs.push(this.pos);
	},
	
	move:function(step)
	{
		this.pos = cc.p(Math.round(this.pos.x+step*Math.sin(Math.radians(this.heading))), 
			Math.round(this.pos.y+step*Math.cos(Math.radians(this.heading))));
	},
	
	getVertexs:function()
	{
		return this.vertexs;
	}
}
);

DashedLineSprite = cc.Sprite.extend(
    {
        _dashArray:null,
        _startX:null,
        _endX:null,
        _startY:null,
        _endY:null,
        ctor:function (startX,startY,endX,endY,dashArray)
        {
            this._super();

			/*
            this._startX= startX * cc.CONTENT_SCALE_FACTOR;
            this._startY =startY * cc.CONTENT_SCALE_FACTOR * -1;
            this._endX =  endX * cc.CONTENT_SCALE_FACTOR;
            this._endY =endY * cc.CONTENT_SCALE_FACTOR * -1;
            */
            this._startX= startX * 2;
            this._startY =startY * 2 * -1;
            this._endX =  endX * 2;
            this._endY =endY * 2 * -1;
            
            if (!dashArray)
            {
                this._dashArray=[10,5];
            }
            else
            {
                this._dashArray=dashArray;
            }
        },
        draw:function () {
			cc.log("draw begin");
            cc.renderContext.fillStyle = "rgba(255,255,255,1)";
            cc.renderContext.strokeStyle = "rgba(255,255,255,1)";
            cc.renderContext.beginPath();

            var x=this._startX;
            var y=this._startY;
            var x2=this._endX;
            var y2=this._endY;

            if(dashLength===0)
            {
                dashLength = 0.001;
            } // Hack for Safari
            var dashCount = this._dashArray.length;

            cc.renderContext.moveTo(x , y );
            var dx = (x2-x), dy = (y2-y);
            var slope = dy/dx;
            var distRemaining = Math.sqrt( dx * dx + dy * dy );
            var dashIndex=0, draw=true;
            while (distRemaining>=0.1)
            {
                var dashLength = this._dashArray[dashIndex++ % dashCount];
                if (dashLength > distRemaining)
                {
                    dashLength = distRemaining;
                }
                var xStep = Math.sqrt( dashLength*dashLength / (1 + slope*slope) );
                if (dx<0)
                {
                    xStep = -xStep;
                }
                x += xStep;
                y += slope*xStep;
                if (draw)
                {
                    cc.renderContext.lineTo(x ,y );
                }
                else
                {
                    cc.renderContext.moveTo(x ,y );
                }
                distRemaining -= dashLength;
                draw = !draw;
            }
            cc.renderContext.closePath();
            cc.renderContext.stroke();
        }
    }
);


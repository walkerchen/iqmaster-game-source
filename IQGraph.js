var IQGraphBase = IQ.extend(
{
	level: 1,
	color: cc.c4f(0.3,0,0,1),
	fillColor: cc.c4f(0,0.5,0,0),
	// 旋转的canvas占形成的圆的半径，相对于cellcontentsize
	radius: 0,
	// 图形的起点离圆心距离
	r1: 0,
	// 图形的终点离圆心距离
	r2: 0,
	// 圆心位置
	center: null,
	
	start: 0,
	end: 1,

	ctor:function(_parent, start, end)
	{
		this._super(_parent);
		this.init(start, end);
		this.explain = (this.lang==LANG_CHS) ? "图形基础" : "Graph base";
	},
	
	myinit:function(start, end)
	{
	},
	
	init:function(start, end)
	{
		cc.log("IQGraphBase init");
		this.start = start;
		this.end = end;
		this.radius = this.parent.cellContentSize.x/2;
		this.r1 = start*this.radius;
		this.r2 = end*this.radius;
		this.center = cc.p(this.parent.cellContentSize.x/2, this.parent.cellContentSize.y/2);
		//cc.log(this.r2);
		this.myinit(start, end);
	},
	
	test:function()
	{
		cc.log("test from IQGraphBox");
	},
	
	// 主要画图函数
	cellGraph:function()
	{
		var pen = new DrawUtil();
		pen.color = this.color;
		pen.fillColor = this.fillColor;
		
		var angle = 45;
		var p1 = cc.p(this.center.x+this.r1*Math.cos(Math.radians(angle)), this.center.y+this.r1*Math.sin(Math.radians(angle)));
		var p2 = cc.p(this.center.x+this.r2*Math.cos(Math.radians(angle)), this.center.y+this.r2*Math.sin(Math.radians(angle)));
		var w = Math.abs(p2.x-p1.x);
		var h = Math.abs(p2.y-p1.y);
			
		var obj = pen.drawRectangle(w,h);
		obj.setAnchorPoint( cc.p(0, 0) );
		obj.setPosition(p1);
		
		return obj;	
	},
	
	newCell:function(badSeed)
	{
		cc.log("newCell base " + badSeed);
		//var txtNum = cc.LabelTTF.create(""+series[i], "Arial", Math.floor(this.parent.cellSize.x/2/maxDigits));
		//var holder = cc.LayerColor.create(cc.c4b(255, 255, 0, 255), this.parent.cellContentSize.x, this.parent.cellContentSize.y);
		var holder = cc.Layer.create();
		//holder.setVisible( true );

		holder.setContentSize(cc.size(this.parent.cellContentSize.x, this.parent.cellContentSize.y));
		holder.setAnchorPoint( cc.p(0.5, 0.5) );
		holder.setPosition( cc.p(0-this.parent.cellSize.x/2, 0-this.parent.cellSize.y/2) );

		holder.addChild(this.cellGraph(badSeed));
		return holder;
	}
}
);

var IQGraphBox = IQGraphBase.extend(
{
	// 主要画图函数
	cellGraph:function()
	{
		var pen = new DrawUtil();
		pen.color = this.color;
		pen.fillColor = this.fillColor;
		
		var angle = 45;
		var p1 = cc.p(this.center.x+this.r1*Math.cos(Math.radians(angle)), this.center.y+this.r1*Math.sin(Math.radians(angle)));
		var p2 = cc.p(this.center.x+this.r2*Math.cos(Math.radians(angle)), this.center.y+this.r2*Math.sin(Math.radians(angle)));
		var w = Math.abs(p2.x-p1.x);
		var h = Math.abs(p2.y-p1.y);
			
		var obj = pen.drawRectangle(w,h);
		obj.setAnchorPoint( cc.p(0, 0) );
		obj.setPosition(p1);
		
		return obj;
	}
});

var IQGraphLine = IQGraphBase.extend(
{
	cellGraph:function()
	{
		var pen = new DrawUtil();
		pen.color = this.color;
		pen.fillColor = this.fillColor;
		
		var angle = 45;
		var p1 = cc.p(this.center.x+this.r1*Math.cos(Math.radians(angle)), this.center.y+this.r1*Math.sin(Math.radians(angle)));
		var p2 = cc.p(this.center.x+this.r2*Math.cos(Math.radians(angle)), this.center.y+this.r2*Math.sin(Math.radians(angle)));
			
		var obj = pen.drawLine(p1,p2);
		obj.setAnchorPoint( cc.p(0, 0) );
		//obj.setPosition(p1);
		
		return obj;
	}
}
);

var IQGraphCircle = IQGraphBase.extend(
{
	cellGraph:function()
	{
		var pen = new DrawUtil();
		pen.color = this.color;
		pen.fillColor = this.fillColor;
		
		var angle = 45;
		var p1 = cc.p(this.center.x+this.r1*Math.cos(Math.radians(angle)), this.center.y+this.r1*Math.sin(Math.radians(angle)));
		var p2 = cc.p(this.center.x+this.r2*Math.cos(Math.radians(angle)), this.center.y+this.r2*Math.sin(Math.radians(angle)));
			
		var r = Math.sqrt(Math.pow(p2.x-p1.x,2) + Math.pow(p2.y-p1.y, 2))/2;
		var obj = pen.drawCircle(r);
		obj.setAnchorPoint( cc.p(0, 0) );
		obj.setPosition(cc.p(p1.x+(p2.x-p1.x)/2,p1.y+(p2.y-p1.y)/2));
		//obj.setPosition(p1);
		
		return obj;
	}
}
);

var IQGraphKoch = IQGraphBase.extend(
{
	repeats: 1,
	logo: null,
	
	myinit:function(start, end)
	{
		this.logo = new IQLogo(this.parent, this.parent.cellContentSize);
	},
	
	drawKoch:function(step, d)
	{
		if(d===0)
			this.logo.fd(step);
		else
		{
			this.drawKoch(step/3, d-1);
			this.logo.rt(60);
			this.drawKoch(step/3, d-1);
			this.logo.rt(-120);
			this.drawKoch(step/3, d-1);
			this.logo.rt(60);
			this.drawKoch(step/3, d-1);
		}
	},
	
	cellGraph:function()
	{
		this.logo.reset();

		var pen = new DrawUtil();
		pen.color = this.color;
		pen.fillColor = this.fillColor;
		
		var angle = 45;
		var p1 = cc.p(this.center.x+this.r1*Math.cos(Math.radians(angle)), this.center.y+this.r1*Math.sin(Math.radians(angle)));
		var p2 = cc.p(this.center.x+this.r2*Math.cos(Math.radians(angle)), this.center.y+this.r2*Math.sin(Math.radians(angle)));
		
		/*
		var turnDeg = 117;
		for(var i=10; i<80; i+=2)
		{
			logo.rt(turnDeg);
			logo.fd(i);
		}
		*/
		
		var len = Math.sqrt(Math.pow(p2.x-p1.x, 2) + Math.pow(p2.y-p1.y, 2));
		this.logo.move(0-len/2);
		this.drawKoch(len, 3 );
		
		var verts = this.logo.getVertexs();
		
		var obj = pen.drawVertexs(verts);
		obj.setAnchorPoint( cc.p(0, 0) );
		//obj.setPosition(p1);
		return obj;
	}
}
);

var IQGraphSpiral = IQGraphBase.extend(
{
	repeats: 1,
	logo: null,
	rightturn: true,
	
	myinit:function(start, end)
	{
		this.logo = new IQLogo(this.parent, this.parent.cellContentSize);
	},
	
	cellGraph:function()
	{
		this.logo.reset();

		var pen = new DrawUtil();
		pen.color = this.color;
		pen.fillColor = this.fillColor;
		
		var angle = 45;
		var p1 = cc.p(this.center.x+this.r1*Math.cos(Math.radians(angle)), this.center.y+this.r1*Math.sin(Math.radians(angle)));
		var p2 = cc.p(this.center.x+this.r2*Math.cos(Math.radians(angle)), this.center.y+this.r2*Math.sin(Math.radians(angle)));
		
		var len = Math.sqrt(Math.pow(p2.x-p1.x, 2) + Math.pow(p2.y-p1.y, 2)) * 0.8;

		var turnDeg = Math.floor(Math.random()*90)+50;
		for(var i=0; i<len/2; i+=3)
		{
			if(this.rightTurn)
				this.logo.rt(turnDeg);
			else
				this.logo.lt(turnDeg);
			this.logo.fd(i);
		}
		
		var verts = this.logo.getVertexs();
		
		var obj = pen.drawVertexs(verts);
		obj.setAnchorPoint( cc.p(0, 0) );
		//obj.setPosition(p1);
		return obj;
	}
}
);

var IQGraphDextroLevo = IQGraphBase.extend(
{
	ctor:function(_parent, start, end)
	{
		this._super(_parent, start, end);
		//this.init(start, end);
		cc.log("level=" + this.level);
		this.explain = (this.lang==LANG_CHS) ? "左旋右旋图形" : "Dextro-levo graphics";
	},
	
	init:function(start, end)
	{
		// Call super's init function
		IQGraphBase.prototype.init.call(this, start, end);
		//cc.log(this._super);
	},
	
	test:function()
	{
		cc.log("test from IQGraphDextroLevo");
	},
	
	generate:function()
	{
		var result = [];
		
		var leftturn = (Math.random()>0.5);
		this.badseed=Math.floor((Math.random()*this.num));
		for(var i = 0; i<this.num; i++) 
		{
			var holder = this.newCell( leftturn && (i==this.badseed) );
			result.push(holder);
		}
		return result;
	}
}
);

var LogoFactors = cc.Class.extend(
{
	// 右转
	rightTurn: true,
	// 螺旋为步距递增，否则步距唯一
	spiral: true,
	//等步距
	equalstep: true,
	// 步距
	step: 3,
	// 形状接近奇数角
	odds: true,
	// 每次旋转角度
	degree: 119,
	// 每次角度固定或递增
	equaldegree: true,
	// 如果spiral=false，是否为闭合形状
	close: true,
	// 颜色一致
	unicolor: true,
	// 是否分段 （是否有moveto）
	segments: 1,
	// 分形图
	recursion: false,
	// 缺省leg
	leg: 50
}
);

// 变化的logo图形
var IQGraphLogo = IQGraphBase.extend(
{
	repeats: 1,
	logo: null,
	factors: null,
	
	myinit:function(start, end)
	{
		this.logo = new IQLogo(this.parent, this.parent.cellContentSize);
		this.factors = new LogoFactors();
	},
	
	recur1:function(loop)
	{
		var empty;
		if(loop<1)
			this.logo.fd(3);
		else
		{
			//empty = this.factors.rightTurn ? this.logo.rt(this.factors.degree) : this.logo.lt(this.factors.degree);
			this.recur(loop-1);
			empty = this.factors.rightTurn ? this.logo.rt(60) : this.logo.lt(60);
			this.recur(loop-1);
			empty = this.factors.rightTurn ? this.logo.lt(120) : this.logo.rt(120);
			this.recur(loop-1);
			empty = this.factors.rightTurn ? this.logo.rt(60) : this.logo.lt(60);
			this.recur(loop-1);
		}
	},
	
	recur2:function(loop, len)
	{
		var empty;
		if(loop<1)
			this.logo.fd(len);
		else
		{
			empty = this.factors.rightTurn ? this.logo.lt(90) : this.logo.rt(90);
			this.recur2(loop-1, len);
			empty = this.factors.rightTurn ? this.logo.rt(90) : this.logo.lt(90);
			this.recur2(loop-1, len);
			empty = this.factors.rightTurn ? this.logo.rt(90) : this.logo.lt(90);
			this.recur2(loop-1, len);
			this.recur2(loop-1, len);
			empty = this.factors.rightTurn ? this.logo.rt(90) : this.logo.lt(90);
			this.recur2(loop-1, len);
			empty = this.factors.rightTurn ? this.logo.rt(90) : this.logo.lt(90);
			this.recur2(loop-1, len);
			empty = this.factors.rightTurn ? this.logo.rt(90) : this.logo.lt(90);
			this.recur2(loop-1, len);
			this.logo.fd(1.5*len/5);
		}
	},
	
	recur:function(loop, len, angle, cmds)
	{
		var empty;
		if(loop<1)
			this.logo.fd(len);
		else
		{
			for(var key in cmds)
			{
				eval(cmds[key]);
			}
		}
	},
	
	cellGraph:function()
	{
		this.logo.reset();

		var pen = new DrawUtil();
		pen.color = this.color;
		pen.fillColor = this.fillColor;
		
		var angle = 45;
		var p1 = cc.p(this.center.x+this.r1*Math.cos(Math.radians(angle)), this.center.y+this.r1*Math.sin(Math.radians(angle)));
		var p2 = cc.p(this.center.x+this.r2*Math.cos(Math.radians(angle)), this.center.y+this.r2*Math.sin(Math.radians(angle)));
		
		var len = Math.sqrt(Math.pow(p2.x-p1.x, 2) + Math.pow(p2.y-p1.y, 2)) * 0.9;

		/*		
		var turnDeg = Math.floor(Math.random()*90)+50;
		var rightTurn = true;
		for(var i=0; i<len/2; i+=3)
		{
			if(rightTurn)
				this.logo.rt(turnDeg);
			else
				this.logo.lt(turnDeg);
			this.logo.fd(i);
		}
		*/

		/*		
		for(var i=0; i<3; i++)
		{
			this.recur1(4);
			empty = this.factors.rightTurn ? this.logo.rt(-60) : this.logo.lt(-60);
		}
		*/
		
		var candidates = [45, 60, 75, 90, 118, 120, 150];
		var deg = candidates[Math.floor((Math.random()*candidates.length))];
		var zig = Math.floor((Math.random()*3))+2;
		zig = 4;
		var cmds = [];
		
		for(var i=0; i<zig; i++)
		{
			dir = (Math.floor(Math.random() > 0.2)) ? true : false;
			if(!dir) deg = 0-deg;
			var cmd = "this.factors.rightTurn ? this.logo.rt(" + deg + ") : this.logo.lt(" + deg + ");\n";
			cmds.push(cmd);
			cmd = "this.recur(loop-1, len, angle, cmds);";
			cmds.push(cmd);
		}

		this.recur(4, 16, deg, cmds);
		
		var verts = this.logo.getVertexs();
		
		var obj = pen.drawVertexs(verts);
		obj.setAnchorPoint( cc.p(0, 0) );
		//obj.setPosition(p1);
		return obj;
	}
}
);

var IQGraphRotate = IQGraphBase.extend(
{
	ctor:function(_parent, start, end)
	{
		this._super(_parent, start, end);
		//this.init(start, end);
		cc.log("level=" + this.level);
		this.explain = (this.lang==LANG_CHS) ? "图形旋转" : "Rotation of graph";
	},
	
	init:function(start, end)
	{
		// Call super's init function
		IQGraphBase.prototype.init.call(this, start, end);
		//cc.log(this._super);
	},
	
	test:function()
	{
		cc.log("test from IQGraphRotate");
	},
	
	generate:function()
	{
		var result = [];
		
		this.badseed=Math.floor((Math.random()*this.num));
		var startAng = Math.floor((Math.random()*4)) * 90;
		var clockwise = Math.random()>0.5;
		for(var i = 0; i<this.num; i++) 
		{
			//var holder = IQGraphBase.prototype.generate.call(this);
			var holder = this.newCell();
			holder.setVisible(false);
			result.push(holder);
			
			var rotAng = 90 * ((i==this.badseed) ? (i+Math.floor((Math.random()*3)+1)) : i) + startAng;
			var act = cc.RotateTo.create(0, (clockwise?rotAng: 360-(rotAng%360)));
			
			var fireLoopStart = cc.CallFunc.create(this.onStart, holder);
			var fireLoopEnd = cc.CallFunc.create(this.onComplete, holder);
			holder.runAction(cc.Sequence.create(fireLoopStart, act, fireLoopEnd));
		}
		return result;
	}
}
);

var IQEngineRotateBox = IQGraphRotate.extend(
{
	pen: null,
	
	ctor:function(_parent, start, end)
	{
		this._super(_parent, start, end);
		this.pen = new IQGraphBox(_parent, start, end);
		this.explain = (this.lang==LANG_CHS) ? "矩形旋转" : "Rotation of box";
	},
	
	// 主要画图函数
	cellGraph:function()
	{
		return this.pen.cellGraph();
	}
}
);

var IQEngineRotateLine = IQGraphRotate.extend(
{
	pen: null,
	
	ctor:function(_parent, start, end)
	{
		this._super(_parent, start, end);
		this.pen = new IQGraphLine(_parent, start, end);
		this.explain = (this.lang==LANG_CHS) ? "线段旋转" : "Rotation of line";
	},
	
	// 主要画图函数
	cellGraph:function()
	{
		return this.pen.cellGraph();
	}
}
);

var IQEngineRotateCircle = IQGraphRotate.extend(
{
	pen: null,
	
	ctor:function(_parent, start, end)
	{
		this._super(_parent, start, end);
		this.pen = new IQGraphCircle(_parent, start, end);
		this.explain = (this.lang==LANG_CHS) ? "圆圈旋转" : "Rotation of circle";
	},
	
	// 主要画图函数
	cellGraph:function()
	{
		return this.pen.cellGraph();
	}
}
);

var IQEngineRotateKoch = IQGraphRotate.extend(
{
	pen: null,
	
	ctor:function(_parent, start, end)
	{
		this._super(_parent, start, end);
		this.pen = new IQGraphKoch(_parent, start, end);
		this.explain = (this.lang==LANG_CHS) ? "雪花旋转" : "Rotation of snow flake";
	},
	
	// 主要画图函数
	cellGraph:function()
	{
		return this.pen.cellGraph();
	}
}
);

var IQEngineRotateSpiral = IQGraphRotate.extend(
{
	pen: null,
	
	ctor:function(_parent, start, end)
	{
		this._super(_parent, start, end);
		this.pen = new IQGraphSpiral(_parent, start, end);
		this.explain = (this.lang==LANG_CHS) ? "螺旋旋转" : "Rotation of spiral";
	},
	
	// 主要画图函数
	cellGraph:function()
	{
		return this.pen.cellGraph();
	}
}
);

var IQEngineDextroLevoSpiral = IQGraphDextroLevo.extend(
{
	pen: null,
	
	ctor:function(_parent, start, end)
	{
		this._super(_parent, start, end);
		this.pen = new IQGraphSpiral(_parent, start, end);
		this.explain = (this.lang==LANG_CHS) ? "螺旋" : "Spiral";
	},
	
	// 主要画图函数
	cellGraph:function(badSeed)
	{
		cc.log("cellGraph IQEngineDextroLevoSpiral");

		if(badSeed) 
			this.pen.rightTurn = false;
		else
			this.pen.rightTurn = true;
		return this.pen.cellGraph();
	}
}
);

var IQGraphRotateCombLineBox_outdated = IQGraphRotate.extend(
{
	linePen: null,
	boxPen: null,
	
	ctor:function(_parent, start, end)
	{
		this._super(_parent, start, end);
		this.linePen = new IQGraphLine(_parent, 0.2, 0.9);
		this.linePen.color = cc.c4f(0,0.6,0,1);
		this.boxPen = new IQGraphBox(_parent, start, end);
		this.boxPen.color = cc.c4f(0,0,0.6,1);
		this.explain = (this.lang==LANG_CHS) ? "线段和矩形组合旋转" : "Rotation of combination";
	},
	
	generate:function()
	{
		var result = [];
		
		this.badseed=Math.floor((Math.random()*this.num));
		var startAng = Math.floor((Math.random()*4)) * 90;
		var clockwise = Math.random()>0.5;
		for(var i = 0; i<this.num; i++) 
		{
			//var holder = cc.LayerColor.create(cc.c4b(255, 0, 0, 255), this.parent.cellContentSize.x, this.parent.cellContentSize.y);
			var holder = cc.Layer.create();
			holder.setContentSize(cc.size(this.parent.cellContentSize.x, this.parent.cellContentSize.y));
			holder.setAnchorPoint( cc.p(0.5, 0.5) );
			holder.setPosition( cc.p(0-this.parent.cellSize.x/2, 0-this.parent.cellSize.y/2) );

			var line = this.linePen.newCell();
			holder.addChild(line);
			var box = this.boxPen.newCell();
			holder.addChild(box);
			
			//holder.setVisible(false);
			result.push(holder);
			
			var rotAng = 90 * ((i==this.badseed) ? (i+Math.floor((Math.random()*3)+1)) : i) + startAng;
			var act = cc.RotateTo.create(0, (clockwise?rotAng: 360-(rotAng%360)));
			
			var fireLoopStart = cc.CallFunc.create(this.onStart, holder);
			var fireLoopEnd = cc.CallFunc.create(this.onComplete, holder);
			//holder.runAction(cc.Sequence.create(fireLoopStart, act, fireLoopEnd));
		}
		
		return result;
	}
}
);

var IQGraphRotateComb = IQGraphRotate.extend(
{
	pens: [],
	
	ctor:function(_parent, start, end)
	{
		this._super(_parent, start, end);
		this.explain = (this.lang==LANG_CHS) ? "线段和矩形组合旋转" : "Rotation of combination";
	},
	
	addGraph:function(graph)
	{
		this.pens.push(graph);
	},
	
	newCell:function()
	{
		var i, o;
		var result = [];
		
		this.badseed=Math.floor((Math.random()*this.num));
		var badObj=Math.floor((Math.random()*this.pens.length));
		var startAng = [];
		var clockwise = [];
		for(o=0; o<this.pens.length; o++)
		{
			startAng[o] = Math.floor((Math.random()*4)) * 90;
			clockwise[o] = Math.random()>0.5;
		}
		for(i = 0; i<this.num; i++) 
		{
			var holder = cc.LayerColor.create(cc.c4b(255, 0, 0, 0), this.parent.cellContentSize.x, this.parent.cellContentSize.y);
			//var holder = cc.Layer.create();
			holder.setContentSize(cc.size(this.parent.cellContentSize.x, this.parent.cellContentSize.y));
			holder.setAnchorPoint( cc.p(0.5, 0.5) );
			holder.setPosition( cc.p(0-this.parent.cellSize.x/2, 0-this.parent.cellSize.y/2) );

			for(o=0; o<this.pens.length; o++)
			{
				var normAng = 90 * i + startAng[o];
				var badAng = normAng + Math.floor((Math.random()*3)+1) * 90;
				//var rotAng = 90 * ((i==this.badseed) ? (i+Math.floor((Math.random()*3)+1)) : i) + startAng;
				var actNorm = cc.RotateTo.create(1.0, (clockwise[o]?normAng: 360-(normAng%360)));
				var actBad = cc.RotateTo.create(1.0, (clockwise[o]?badAng: 360-(badAng%360)));
			

				var holderObj = cc.LayerColor.create(cc.c4b(0, 0, 0, 0), this.parent.cellContentSize.x, this.parent.cellContentSize.y);
				holderObj.setVisible(true);
				holderObj.setContentSize(cc.size(this.parent.cellContentSize.x, this.parent.cellContentSize.y));
				holderObj.setAnchorPoint( cc.p(0.5, 0.5) );
				holderObj.setPosition( cc.p(0,0));
				
				var obj = this.pens[o].newCell();
				obj.setPosition(cc.p(0, 0));
				obj.setAnchorPoint(cc.p(0, 0));
				holderObj.addChild(obj);
				
				var fireLoopStart = cc.CallFunc.create(this.onStart, holderObj);
				var fireLoopEnd = cc.CallFunc.create(this.onComplete, holderObj);

				if(i==this.badseed && o==badObj)
				{
					holderObj.runAction(cc.Sequence.create(fireLoopStart, actBad, fireLoopEnd));
				}
				else
				{
					holderObj.runAction(cc.Sequence.create(fireLoopStart, actNorm, fireLoopEnd));
				}
				
				holder.addChild(holderObj);
			}

			result.push(holder);
		}
		
		return result;
	}
}
);

var IQEngineRotateComb = IQGraphRotateComb.extend(
{
	ctor:function(_parent, start, end)
	{
		this._super(_parent, start, end);
		this.explain = (this.lang==LANG_CHS) ? "线段和矩形组合旋转" : "Rotation of combination";
		
		var linePen = new IQGraphLine(_parent, start, end);
		linePen.color = cc.c4f(0,0.6,0,1);
		this.addGraph(linePen);
		
		var boxPen = new IQGraphBox(_parent, start, end);
		boxPen.color = cc.c4f(0,0,0.6,1);
		this.addGraph(boxPen);

		var boxCircle = new IQGraphCircle(_parent, start, end);
		boxCircle.color = cc.c4f(0.6,0,0,1);
		this.addGraph(boxCircle);
	},
	
	generate:function()
	{
		return this.newCell();
	}
});

//
// NewGameScene class
//

cc.log("NewGameScene is running...");

var NewGameScene = function (){
	this.board = new IQDraw(this);
	this.gridSize = cc.size(4, 8);
};

NewGameScene.prototype.onDidLoadFromCCB = function()
{
	cc.log("NewGameScene onDidLoadFromCCB " + this.rootNode);

	this.rootNode.schedule(function (dt) {  
		this.controller.onUpdate(dt);  
    });  
    
    this.rootNode.onExit = function () {  
    };  
  
    this.rootNode.onTouchesBegan = function (touches, event) {  
        return true;  
    };  
  
    this.rootNode.onTouchesMoved = function (touches, event) {  
        return true;  
    };  
    this.rootNode.onTouchesEnded = function (touches, event) {  

		//cc.log("Touch ended" + this.controller.toSource());
		this.controller.board.findTouchedItem(touches, event);
        return true;  
    };  
    this.rootNode.setTouchEnabled(true);

	//this.initMatrix();
	this.testDraw();
};

NewGameScene.prototype.testDraw = function ()
{
	this.board.play(); 
	//this.board.hello();
};

NewGameScene.prototype.newColorBox = function(x, y, rect)
{
	//var box = cc.Sprite.create("box_"+x+"_"+y, rect);
	//var txt = 
	//var box = cc.Sprite.createWithTexture(txt, rect);
	//var box = cc.Sprite();
	var box = cc.LayerColor.create(cc.c4b(Math.floor(Math.random()*256),Math.floor(Math.random()*256),60,255), rect.width, rect.height);
	//cc.log("box=" + box);
	return box;
};

NewGameScene.prototype.initMatrix = function()
{
	var layerSize = this.gameLayer.getContentSize();
	var cellSize = cc.size(parseInt(layerSize.width/gridSize.width, 0), parseInt(layerSize.height/gridSize.height, 0));
	
	cc.log("initMatrix layerSize(" + layerSize.width + "," + layerSize.height + ")");

	// Create color matrix
	for(var i=0; i<this.gridSize.width; i++)
	{
		for(var j=0; j<this.gridSize.height; j++)
		{
			//var colorBox = cc.LayerColor.create(cc.c4b(Math.floor(Math.random()*256),Math.floor(Math.random()*256),60,255), cellSize.width-1, cellSize.height-1);
			var colorBox = this.newColorBox( i, j, cc.rect(0,0,cellSize.width-1, cellSize.height-1));
			colorBox.setPosition( cc.p(i*cellSize.width+1,j*cellSize.height+1)); 
			colorBox.setAnchorPoint( cc.p(0,0));
			this.gameLayer.addChild(colorBox, -1);
		}
	}

/*
	var sprite1 = cc.Sprite.create("clickme.png");
	sprite1.setPosition(cc.p(10,10));
	this.gameLayer.addChild(sprite1); 
*/
};

// Create callback for button
NewGameScene.prototype.onBack = function()
{	
	cc.FileUtils.getInstance().loadFilenameLookup("fileLookup.plist");
    //this.initMatrix();
    //return;
    
	var director = cc.Director.getInstance();
    var scene = cc.BuilderReader.loadAsScene("Scenes/MainMenuScene");
    var runningScene = director.getRunningScene();
    if (runningScene === null) director.runWithScene(scene);
    else director.replaceScene(scene);
};

NewGameScene.prototype.onUpdate = function()
{
	//this.initMatrix();
};


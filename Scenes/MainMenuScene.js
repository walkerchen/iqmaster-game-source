//
// MainMenuScene class
//
var LANG_CHS = 1;
var LANG_ENG = 2;

cc.SPRITE_DEBUG_DRAW = 1;

var gWinSize = cc.Director.getInstance().getWinSize();
var gScaleFactor;
if (gWinSize.width <= 320) gScaleFactor = 1;
else gScaleFactor = 2;

var MainMenuScene = function(){};

// Create callback for button
MainMenuScene.prototype.onNewGame = function()
{	
	this.appTitle.runAction(cc.RotateBy.create(1,360));
	
	cc.FileUtils.getInstance().loadFilenameLookup("fileLookup.plist");
    //cc.Texture2D.setDefaultAlphaPixelFormat(6);
	var director = cc.Director.getInstance();
    var scene = cc.BuilderReader.loadAsScene("Scenes/NewGameScene");
    var runningScene = director.getRunningScene();
    if (runningScene === null) director.runWithScene(scene);
    else director.replaceScene(scene);
};

